@XmlJavaTypeAdapters(
        @XmlJavaTypeAdapter(value = ZonedDateTimeAdapter.class, type = ZonedDateTime.class )
)
package com.sirius.xm.soaprestdemo.dto;


import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.ZonedDateTime;
import com.sirius.xm.soaprestdemo.adapter.ZonedDateTimeAdapter;