package com.sirius.xm.soaprestdemo.config;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.ws.config.annotation.EnableWs;

import com.sirius.xm.soaprestdemo.controller.BaseDataController;
import com.sirius.xm.soaprestdemo.controller.BaseDataSoapController;

import javax.xml.ws.Endpoint;

@Configuration
@EnableWs
@Profile("soap")
public class WebServicesConfig {

    @Bean
    public BaseDataSoapController baseDataWebService(BaseDataController controller) {
        return new BaseDataSoapController(controller);
    }

    @Bean
    public ServletRegistrationBean wsDispatcherServlet() {
        CXFServlet cxfServlet = new CXFServlet();
        return new ServletRegistrationBean(cxfServlet, "/services/*");
    }

    @Bean(name= Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint stammdatenEndpoint(SpringBus springBus, BaseDataSoapController baseDataSoapService) {
        EndpointImpl endpoint = new EndpointImpl(springBus, baseDataSoapService);
        endpoint.publish("/baseDataWs");
        return endpoint;
    }

}


