package com.sirius.xm.soaprestdemo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sirius.xm.soaprestdemo.dto.BasedataResponseDto;
import com.sirius.xm.soaprestdemo.dto.GeneralAgreementDto;
import com.sirius.xm.soaprestdemo.dto.PermanentEstablishmentDto;
import com.sirius.xm.soaprestdemo.exception.BaseDataServiceException;
import com.sirius.xm.soaprestdemo.model.GeneralAgreement;
import com.sirius.xm.soaprestdemo.model.PermanentEstablishment;
import com.sirius.xm.soaprestdemo.repository.GenrealAgreementRepository;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Transactional
public class BaseDataController {

	private static final Logger log = LoggerFactory.getLogger(BaseDataController.class);

	@Autowired
	private GenrealAgreementRepository generalAgreementRepository;

	public BasedataResponseDto getBaseData(String masterAgreementName) throws BaseDataServiceException {
		log.info("Get Master Data for {}", masterAgreementName);
		GeneralAgreement generalAgreement = generalAgreementRepository.findByName(masterAgreementName);
		if (generalAgreement == null) {
			throw new BaseDataServiceException();
		}
		BasedataResponseDto responseDto = new BasedataResponseDto();
		responseDto.setGeneralAgreementDto(mapToDto(generalAgreement));
		responseDto.setPermanentEstablishmentDtos(mapToDtos(generalAgreement.getPermenentEstablishments()));
		return responseDto;
	}

	private Set<PermanentEstablishmentDto> mapToDtos(Set<PermanentEstablishment> permenentEstablishment) {
		if (permenentEstablishment == null) {
			return Collections.emptySet();
		} else {
			return permenentEstablishment.stream().map(this::mapToDto).collect(Collectors.toSet());
		}
	}

	public Long postPermenentEstablishment(PermanentEstablishmentDto permenenetEstablishment,
			String masterAgreementName) throws BaseDataServiceException {

		GeneralAgreement generalAgreement = generalAgreementRepository.findByName(masterAgreementName);

		if (generalAgreement == null) {
			throw new BaseDataServiceException();
		}

		PermanentEstablishment bs = new PermanentEstablishment();
		bs.setRecognisation(permenenetEstablishment.getRecognisation());
		bs.setUniqueKey(permenenetEstablishment.getUniqueKey());
		bs.setDateOfExpiry(permenenetEstablishment.getZonedDateTime());
		generalAgreement.getPermenentEstablishments().add(bs);
		generalAgreementRepository.save(generalAgreement);

		return bs.getId();

	}

	public GeneralAgreementDto mapToDto(GeneralAgreement generalAgreement) {
		GeneralAgreementDto dto = new GeneralAgreementDto();
		dto.setName(generalAgreement.getName());
		dto.setTyp(generalAgreement.getTyp());
		return dto;
	}

	public PermanentEstablishmentDto mapToDto(PermanentEstablishment permentEstablishment) {
		PermanentEstablishmentDto dto = new PermanentEstablishmentDto();
		dto.setRecognisation(permentEstablishment.getRecognisation());
		dto.setZonedDateTime(permentEstablishment.getDateOfExpiry());
		dto.setUniqueKey(permentEstablishment.getUniqueKey());
		return dto;
	}

}
