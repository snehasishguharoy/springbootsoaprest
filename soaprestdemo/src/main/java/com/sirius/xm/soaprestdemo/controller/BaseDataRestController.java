package com.sirius.xm.soaprestdemo.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.sirius.xm.soaprestdemo.dto.BasedataResponseDto;
import com.sirius.xm.soaprestdemo.dto.PermanentEstablishmentDto;
import com.sirius.xm.soaprestdemo.exception.BaseDataServiceException;

@RestController
public class BaseDataRestController {

	private static final Logger log = LoggerFactory.getLogger(BaseDataRestController.class);

	@Autowired
	private BaseDataController baseDataController;

	@ApiOperation(value = "Returns master data about the caller.", nickname = "getBaseData", httpMethod = "GET", produces = "application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = BasedataResponseDto.class),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Failure") })
	@GetMapping("/v1/api/basedata")
	public ResponseEntity<BasedataResponseDto> getBaseData(
			@ApiParam("Name of the framework contract") @RequestParam(required = false, name = "name") String generalAgreementName)
			throws BaseDataServiceException {

		log.info("Get Master data for {}", generalAgreementName);

		try {
			return ResponseEntity.ok(baseDataController.getBaseData(generalAgreementName));
		} catch (BaseDataServiceException sse) {
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("/v1/api/stammdaten/{masteragreementname}")
	public ResponseEntity<Void> postPermenentEstablishment(@RequestBody PermanentEstablishmentDto permenentEstablishment,
			@PathVariable("masteragreementname") String masterAgreementName,
			UriComponentsBuilder uriComponentsBuilder) {

		try {
			baseDataController.postPermenentEstablishment(permenentEstablishment, masterAgreementName);
			return ResponseEntity.created(uriComponentsBuilder.build().toUri()).build();
		} catch (BaseDataServiceException e) {
			return ResponseEntity.notFound().build();
		}
	}

}
