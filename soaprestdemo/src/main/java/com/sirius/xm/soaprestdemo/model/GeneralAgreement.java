package com.sirius.xm.soaprestdemo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
public class GeneralAgreement extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    private String name;
    
    private String typ;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PermanentEstablishment> permenentEstablishments = new HashSet<>();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeneralAgreement that = (GeneralAgreement) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return typ != null ? typ.equals(that.typ) : that.typ == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (typ != null ? typ.hashCode() : 0);
        return result;
    }
}
