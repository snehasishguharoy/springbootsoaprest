package com.sirius.xm.soaprestdemo;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.sirius.xm.soaprestdemo.model.GeneralAgreement;
import com.sirius.xm.soaprestdemo.model.PermanentEstablishment;
import com.sirius.xm.soaprestdemo.repository.GenrealAgreementRepository;

@SpringBootApplication
public class SoaprestdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoaprestdemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner runAfterStart(final GenrealAgreementRepository repository) {
		return args -> {
			PermanentEstablishment one = new PermanentEstablishment();
			one.setDateOfExpiry(ZonedDateTime.now().plusDays(1));
			one.setRecognisation("ONE");
			one.setUniqueKey(UUID.randomUUID().toString());

			PermanentEstablishment two = new PermanentEstablishment();
			two.setDateOfExpiry(ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS));
			two.setRecognisation("TWO");
			two.setUniqueKey(UUID.randomUUID().toString());

			GeneralAgreement generalAgreement = new GeneralAgreement();
			generalAgreement.setName("RV1");
			generalAgreement.setTyp("TYP");
			generalAgreement.getPermenentEstablishments().add(one);
			generalAgreement.getPermenentEstablishments().add(two);
			repository.save(generalAgreement);

			PermanentEstablishment three = new PermanentEstablishment();
			three.setDateOfExpiry(ZonedDateTime.now().plusDays(1));
			three.setRecognisation("THREE");
			three.setUniqueKey(UUID.randomUUID().toString());

			PermanentEstablishment four = new PermanentEstablishment();
			four.setDateOfExpiry(ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS));
			four.setRecognisation("FOUR");
			four.setUniqueKey(UUID.randomUUID().toString());

			GeneralAgreement generalAgreement2 = new GeneralAgreement();
			generalAgreement2.setTyp("TYP");
			generalAgreement2.setName("RV2");
			generalAgreement2.getPermenentEstablishments().add(three);
			generalAgreement2.getPermenentEstablishments().add(four);

			repository.save(generalAgreement2);

			repository.flush();
		};
	}

}
