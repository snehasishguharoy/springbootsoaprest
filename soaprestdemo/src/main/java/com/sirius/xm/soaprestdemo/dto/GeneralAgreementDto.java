package com.sirius.xm.soaprestdemo.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneralAgreementDto implements Serializable{

    private static final long serialVersionUID = -5106731168761398947L;

    private String typ;

    private String name;



}
