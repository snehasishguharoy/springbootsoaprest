
package com.sirius.xm.soaprestdemo.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sirius.xm.soaprestdemo.adapter.ZonedDateTimeAdapter;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PermanentEstablishmentDto implements Serializable {

	private static final long serialVersionUID = -5857580560880503258L;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", locale = "de-DE", timezone = "Europe/Berlin")
	@XmlJavaTypeAdapter(value = ZonedDateTimeAdapter.class, type = ZonedDateTime.class)
	private ZonedDateTime zonedDateTime;

	private String recognisation;
	private String uniqueKey;

}
