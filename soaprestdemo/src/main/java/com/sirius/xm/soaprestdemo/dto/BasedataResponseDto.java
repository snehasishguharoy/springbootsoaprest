package com.sirius.xm.soaprestdemo.dto;

import java.util.Set;

import lombok.Data;


@Data
public class BasedataResponseDto {

    private GeneralAgreementDto generalAgreementDto;
    private Set<PermanentEstablishmentDto> permanentEstablishmentDtos;

}
