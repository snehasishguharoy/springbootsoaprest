package com.sirius.xm.soaprestdemo.controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sirius.xm.soaprestdemo.dto.BasedataResponseDto;
import com.sirius.xm.soaprestdemo.dto.PermanentEstablishmentDto;
import com.sirius.xm.soaprestdemo.exception.BaseDataServiceException;

@WebService
public class BaseDataSoapController {

	private static final Logger log = LoggerFactory.getLogger(BaseDataSoapController.class);

	private final BaseDataController controller;

	@Autowired
	public BaseDataSoapController(BaseDataController controller) {
		this.controller = controller;
	}

	@WebMethod
	public BasedataResponseDto getBaseData(@WebParam(name = "masterAgreementName") String name)
			throws BaseDataServiceException {
		return controller.getBaseData(name);
	}

	@WebMethod()
	public void addPermenetEstablishment(@WebParam(name = "permenentEstablishment") PermanentEstablishmentDto permenentEstablishment,
			@WebParam(name = "generalAgreement") String masterAgreementName) throws BaseDataServiceException {
		controller.postPermenentEstablishment(permenentEstablishment, masterAgreementName);
	}
}
