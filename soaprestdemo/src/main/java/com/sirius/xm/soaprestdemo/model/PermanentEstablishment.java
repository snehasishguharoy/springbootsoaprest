package com.sirius.xm.soaprestdemo.model;

import java.time.ZonedDateTime;

import javax.persistence.Entity;

import lombok.Data;

@Entity
@Data
public class PermanentEstablishment extends AbstractEntity {

    private ZonedDateTime dateOfExpiry;

    private String recognisation;
    private String uniqueKey;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermanentEstablishment that = (PermanentEstablishment) o;

        return uniqueKey != null ? uniqueKey.equals(that.uniqueKey) : that.uniqueKey == null;
    }

    @Override
    public int hashCode() {
        return uniqueKey != null ? uniqueKey.hashCode() : 0;
    }
}
