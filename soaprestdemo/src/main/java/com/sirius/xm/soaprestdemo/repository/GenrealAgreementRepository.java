package com.sirius.xm.soaprestdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sirius.xm.soaprestdemo.model.GeneralAgreement;

import java.util.List;

public interface GenrealAgreementRepository extends JpaRepository<GeneralAgreement, Long> {

	GeneralAgreement findByName(String name);

	List<GeneralAgreement> findByTyp(String typ);
}
